package br.edu.ifspsaocarlos.sdm.boardgamecounter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import java.math.BigDecimal;

public class DadosActivity extends AppCompatActivity implements View.OnClickListener {

    private final static String TAG = "DadosActivity";

    private ImageView dado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_dados);

        dado = findViewById(R.id.img_dado);

        dado.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        RotateAnimation rotateAnimation = new RotateAnimation(0, 360f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);

        rotateAnimation.setInterpolator(new LinearInterpolator());
        rotateAnimation.setDuration(100);
        rotateAnimation.setRepeatCount(6);
        rotateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                setRandomDiceImage();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                setRandomDiceImage();
            }
        });
        dado.startAnimation(rotateAnimation);

    }

    private void setRandomDiceImage() {
        Integer number = getRandomNumber();
        int resId = -1;
        switch (number) {
            case 1:
                resId = R.drawable.dado1;
                break;
            case 2:
                resId = R.drawable.dado2;
                break;
            case 3:
                resId = R.drawable.dado3;
                break;
            case 4:
                resId = R.drawable.dado4;
                break;
            case 5:
                resId = R.drawable.dado5;
                break;
            case 6:
                resId = R.drawable.dado6;
                break;
        }

        dado.setImageResource(resId);
    }

    @NonNull
    private Integer getRandomNumber() {
        double numberRandom = Math.random() * 5 + 1;
        BigDecimal numberBD = new BigDecimal(numberRandom);
        return numberBD.setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
    }
}
