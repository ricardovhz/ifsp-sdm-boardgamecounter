package br.edu.ifspsaocarlos.sdm.boardgamecounter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;

public class MenuPrincipalActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "MenuPrincipalActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

        Button btnContadores = (Button) findViewById(R.id.btn_contadores);
        Button btnSorteio = (Button) findViewById(R.id.btn_sorteio);

        btnContadores.setOnClickListener(this);
        btnSorteio.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (isBtnContadoresClicked(view)) {
            PopupMenu popup = openPopupMenu(view);

            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    Intent i = null;

                    if (isXadrezItemClicked(item)) {
                        i = new Intent(MenuPrincipalActivity.this, ChessCounterActivity.class);
                    } else {
                        if (isCronometroItemClicked(item)) {
                            i = new Intent(MenuPrincipalActivity.this, CronometerActivity.class);
                        }
                    }
                    startActivity(i);
                    return true;
                }
            });

            popup.show();
        } else {
            if (isBtnSorteioClicked(view)) {
                PopupMenu popupMenu = openPopupMenu(view);

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        Intent i = null;
                        if (isDadosItemClicked(item)) {
                            i = new Intent(MenuPrincipalActivity.this, DadosActivity.class);

                        } else {
                            i = new Intent(MenuPrincipalActivity.this, LotteryActivity.class);
                        }
                        startActivity(i);
                        return true;
                    }
                });

                popupMenu.show();
            }
        }
    }

    private boolean isBtnSorteioClicked(View view) {
        return R.id.btn_sorteio == view.getId();
    }

    private boolean isBtnContadoresClicked(View view) {
        return R.id.btn_contadores == view.getId();
    }

    private boolean isDadosItemClicked(MenuItem item) {
        return R.id.dados == item.getItemId();
    }

    private boolean isCronometroItemClicked(MenuItem item) {
        return R.id.cronometro == item.getItemId();
    }

    private boolean isXadrezItemClicked(MenuItem item) {
        return R.id.xadrez == item.getItemId();
    }

    private PopupMenu openPopupMenu(View view) {
        PopupMenu popup = new PopupMenu(this, view);

        if (isBtnContadoresClicked(view)) {
            popup.getMenuInflater().inflate(R.menu.popup_menu_contadores, popup.getMenu());
        } else {
            if (isBtnSorteioClicked(view)) {
                popup.getMenuInflater().inflate(R.menu.popup_menu_sorteio, popup.getMenu());
            }
        }

        return popup;
    }
}
