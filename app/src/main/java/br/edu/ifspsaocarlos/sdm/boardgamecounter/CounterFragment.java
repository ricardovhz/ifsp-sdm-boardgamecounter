package br.edu.ifspsaocarlos.sdm.boardgamecounter;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CounterFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CounterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CounterFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_ROTATED = "rotated";
    private static final String ARG_MINUTES = "minutes";
    private static final String ARG_SECONDS = "seconds";

    // TODO: Rename and change types of parameters
    private Boolean rotated;

    private OnFragmentInteractionListener mListener;

    private TextView textCounter;
    private int minutes;
    private int seconds;

    public CounterFragment() {
    }

    // TODO: Rename and change types and number of parameters
    public static CounterFragment newInstance(int minutes, int seconds, Boolean rotated) {
        CounterFragment fragment = new CounterFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_ROTATED, rotated);
        args.putInt(ARG_MINUTES, minutes);
        args.putInt(ARG_SECONDS, seconds);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            rotated = getArguments().getBoolean(ARG_ROTATED);
            minutes = getArguments().getInt(ARG_MINUTES);
            seconds = getArguments().getInt(ARG_SECONDS);
        }

        if (rotated == null)
            rotated = Boolean.FALSE;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        return inflater.inflate(R.layout.fragment_counter, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        textCounter = view.findViewById(R.id.text_counter);
        if (rotated) {
            textCounter.setRotation(180f);
        }

        textCounter.setClickable(true);
        textCounter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null)
                    mListener.onTimerClicked(CounterFragment.this);
            }
        });

        updateTimer();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) activity;
        } else {
            throw new RuntimeException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private String pad(int value) {
        StringBuilder sb = new StringBuilder().append("00").append(value);
        return sb.substring(sb.length() - 2);
    }

    public void updateTimer() {
        this.textCounter.setText(pad(minutes) + ":" + pad(seconds));
    }

    public void decrementTimer() {
        if (minutes == 0 && seconds == 0) {
            if (this.mListener != null)
                this.mListener.onTimerFinished();
            return;
        }

        if (seconds == 0) {
            this.seconds = 59;
            this.minutes--;
        } else {
            this.seconds--;
        }

        updateTimer();
    }

    public void incrementTimer() {
        if (seconds == 59) {
            this.seconds = 0;
            this.minutes++;
        } else {
            this.seconds++;
        }

        updateTimer();
    }

    public void reset(int minutes, int seconds) {
        this.minutes = minutes;
        this.seconds = seconds;
        updateTimer();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onTimerClicked(CounterFragment fragment);

        void onTimerFinished();
    }
}
