package br.edu.ifspsaocarlos.sdm.boardgamecounter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.edu.ifspsaocarlos.sdm.boardgamecounter.adapter.ItemAdapter;

public class ItensActivity extends AppCompatActivity implements View.OnClickListener {

    private ListView listItens;
    private List<String> itens;
    private Button btnOk;
    private Button btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_itens);

        this.itens = new ArrayList<>();
        if (getIntent().hasExtra("itens")) {
            String[] itensArray = getIntent().getStringArrayExtra("itens");
            for (int i = 0; i < itensArray.length; i++)
                this.itens.add(itensArray[i]);
        }

        this.btnOk = findViewById(R.id.btn_ok);
        this.btnCancel = findViewById(R.id.btn_cancel);

        this.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ItensActivity.this.setResult(1);
                ItensActivity.this.finish();
            }
        });
        this.btnOk.setOnClickListener(this);

        this.listItens = findViewById(R.id.listItens);
        listItens.setAdapter(new ItemAdapter(this, R.layout.fragment_item, itens));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.itens_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (R.id.menu_item_add == item.getItemId()) {
            NewItemDialog dialog = new NewItemDialog();
            dialog.setListener(new OnAddListener() {
                @Override
                public void add(String item) {
                    ItensActivity.this.itens.add(item);
                    ItensActivity.this.listItens.setAdapter(new ItemAdapter(ItensActivity.this,
                            R.layout.fragment_item, ItensActivity.this.itens));
                    Toast.makeText(ItensActivity.this, R.string.success_add,
                            Toast.LENGTH_SHORT).show();
                }
            });
            dialog.show(getSupportFragmentManager(), "newItem");
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent();
        intent.putExtra("itens", this.itens.toArray(new String[0]));
        setResult(0, intent);
        finish();
    }

    public interface OnAddListener {
        void add(String item);
    }

    public static class NewItemDialog extends DialogFragment {

        private OnAddListener listener;

        public void setListener(OnAddListener listener) {
            this.listener = listener;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            builder.setView(inflater.inflate(R.layout.new_item_dialog, null))
                    .setPositiveButton(R.string.new_item_adicionar, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            TextView txtItem = NewItemDialog.this.getDialog().findViewById(R.id.txtItem);
                            listener.add(txtItem.getText().toString());
                        }
                    })
                    .setNegativeButton(R.string.new_item_cancelar, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            NewItemDialog.this.getDialog().cancel();
                        }
                    });

            return builder.create();
        }
    }

}
