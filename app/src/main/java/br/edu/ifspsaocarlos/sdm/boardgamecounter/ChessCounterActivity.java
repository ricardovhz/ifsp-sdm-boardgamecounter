package br.edu.ifspsaocarlos.sdm.boardgamecounter;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import static br.edu.ifspsaocarlos.sdm.boardgamecounter.ConfigurationActivity.ARG_CONTEXT;
import static br.edu.ifspsaocarlos.sdm.boardgamecounter.ConfigurationActivity.ARG_CONTEXT_CHESS_COUNTER;
import static br.edu.ifspsaocarlos.sdm.boardgamecounter.ConfigurationActivity.ARG_MINUTOS;
import static br.edu.ifspsaocarlos.sdm.boardgamecounter.ConfigurationActivity.ARG_SEGUNDOS;

public class ChessCounterActivity extends AppCompatActivity implements View.OnClickListener, CounterFragment.OnFragmentInteractionListener {

    private CounterFragment contadorCima;
    private CounterFragment contadorBaixo;
    private CounterFragment current;

    private boolean counting;
    private int configMinutes;
    private int configSeconds;

    private Timer timer;
    private Button btnStart;
    private Button btnRestart;
    private Button btnConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_chess_counter);

        this.counting = false;
        this.configMinutes = 1;
        this.configSeconds = 0;

        this.contadorCima = CounterFragment.newInstance(configMinutes, configSeconds, true);
        this.contadorBaixo = CounterFragment.newInstance(configMinutes, configSeconds, false);
        this.current = contadorCima;
        this.btnRestart = findViewById(R.id.btnRestart);

        this.btnRestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!counting) {
                    resetCounters();
                }
            }
        });

        this.btnConfig = findViewById(R.id.btn_chess_counter_config);
        this.btnConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToConfig();
            }
        });

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.contador_cima, this.contadorCima, "contador-cima");
        transaction.add(R.id.contador_baixo, this.contadorBaixo, "contador-baixo");
        transaction.commit();

        this.btnStart = (Button) findViewById(R.id.btnStart);
        btnStart.setOnClickListener(this);
    }

    private void resetCounters() {
        contadorCima.reset(configMinutes, configSeconds);
        contadorBaixo.reset(configMinutes, configSeconds);
    }

    private void goToConfig() {
        Intent intent = new Intent(this, ConfigurationActivity.class);
        intent.putExtra(ARG_CONTEXT, ARG_CONTEXT_CHESS_COUNTER);
        intent.putExtra(ARG_MINUTOS, this.configMinutes);
        intent.putExtra(ARG_SEGUNDOS, this.configSeconds);
        startActivityForResult(intent, 0);
    }

    @Override
    public void onClick(View view) {
        if (!counting) {
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    decrementTimer();
                }
            };
            timer = new Timer();
            timer.schedule(task, 1000L, 1000L);
            iniciarContagem();
        } else {
            timer.cancel();
            pararContagem();
        }
    }

    private void decrementTimer() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                current.decrementTimer();
            }
        });
    }

    @Override
    public void onTimerClicked(CounterFragment fragment) {
        if (counting) {
            if (current == fragment) {
                if (this.current == this.contadorCima)
                    this.current = this.contadorBaixo;
                else
                    this.current = this.contadorCima;
            }
        }
    }

    @Override
    public void onTimerFinished() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pararContagem();
                Toast.makeText(ChessCounterActivity.this, R.string.terminou, Toast.LENGTH_LONG).show();
            }
        });
        timer.cancel();

    }

    private void iniciarContagem() {
        counting = true;
        btnStart.setText(R.string.pause);
        btnRestart.setEnabled(false);
        btnConfig.setEnabled(false);
    }

    private void pararContagem() {
        counting = false;
        btnStart.setText(R.string.iniciar);
        btnRestart.setEnabled(true);
        btnConfig.setEnabled(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            this.configMinutes = data.getIntExtra(ARG_MINUTOS, 1);
            this.configSeconds = data.getIntExtra(ARG_SEGUNDOS, 0);
            resetCounters();
        }
    }
}
