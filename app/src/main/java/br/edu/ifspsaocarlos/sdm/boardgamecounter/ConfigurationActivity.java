package br.edu.ifspsaocarlos.sdm.boardgamecounter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ConfigurationActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String ARG_CONTEXT = "context";
    public static final String ARG_CONTEXT_CHESS_COUNTER = "context-chess-counter";

    public static final String ARG_MINUTOS = "minutos";
    public static final String ARG_SEGUNDOS = "segundos";

    private int minutos;
    private int segundos;
    private TextView txtMinutes;
    private TextView txtSeconds;
    private Button btnOk;
    private Button btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);
        setTitle(getString(R.string.configuracoes_title));

        this.minutos = getIntent().getIntExtra(ARG_MINUTOS, 1);
        this.segundos = getIntent().getIntExtra(ARG_SEGUNDOS, 0);

        if (isChessCounter()) {
            ((LinearLayout) findViewById(R.id.context_chess_counter)).setVisibility(View.VISIBLE);
            this.txtMinutes = findViewById(R.id.txt_duracao_minutos);
            this.txtSeconds = findViewById(R.id.txt_duracao_segundos);
            this.txtMinutes.setText(String.valueOf(this.minutos));
            this.txtSeconds.setText(String.valueOf(this.segundos));
        }

        this.btnOk = findViewById(R.id.btn_ok);
        this.btnOk.setOnClickListener(this);

        this.btnCancel = findViewById(R.id.btn_cancel);
        this.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConfigurationActivity.this.finish();
            }
        });
    }

    private boolean isChessCounter() {
        return getIntent().getStringExtra(ARG_CONTEXT).equals(ARG_CONTEXT_CHESS_COUNTER);
    }

    @Override
    public void onClick(View view) {
        this.minutos = Integer.parseInt(txtMinutes.getText().toString());
        this.segundos = Integer.parseInt(txtSeconds.getText().toString());
        Intent i = new Intent(Intent.ACTION_PICK);
        i.putExtra(ARG_MINUTOS, this.minutos);
        i.putExtra(ARG_SEGUNDOS, this.segundos);
        setResult(0, i);
        finish();
    }

}
