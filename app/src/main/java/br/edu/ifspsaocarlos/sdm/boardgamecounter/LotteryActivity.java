package br.edu.ifspsaocarlos.sdm.boardgamecounter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LotteryActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnItens;
    private ImageView imgRoleta;
    private LinearLayout layoutSorteado;
    private TextView txtSorteado;
    private List<String> itens;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lottery);

        this.itens = new ArrayList<>();

        this.txtSorteado = findViewById(R.id.txt_sorteado);
        this.layoutSorteado = findViewById(R.id.layout_sorteado);

        this.btnItens = findViewById(R.id.btn_itens);
        this.btnItens.setOnClickListener(this);
        this.imgRoleta = findViewById(R.id.img_roleta);
        imgRoleta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doLottery();
            }
        });
    }

    public void doLottery() {
        if (this.itens.isEmpty()) {
            Toast.makeText(this, "É necessário adicionar itens para sortear",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        RotateAnimation rotateAnimation = new RotateAnimation(0, 90f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);

        rotateAnimation.setInterpolator(new LinearInterpolator());
        rotateAnimation.setDuration(100);
        rotateAnimation.setRepeatCount(6);
        rotateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                setRandomItem();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        imgRoleta.startAnimation(rotateAnimation);
    }

    public void setRandomItem() {
        double numberRandom = Math.random() * this.itens.size();
        BigDecimal numberBD = new BigDecimal(numberRandom);
        int position = numberBD.setScale(0, BigDecimal.ROUND_DOWN).intValue();
        String sorteado = this.itens.get(position);
        this.txtSorteado.setText(sorteado);
        if (layoutSorteado.getVisibility() == View.INVISIBLE) {
            layoutSorteado.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        Intent i = new Intent(this, ItensActivity.class);
        i.putExtra("itens", this.itens.toArray(new String[0]));
        startActivityForResult(i, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && resultCode == 0) {
            itens = Arrays.asList(data.getStringArrayExtra("itens"));
            Toast.makeText(this, R.string.itens_sucesso, Toast.LENGTH_SHORT).show();
        }
    }
}
