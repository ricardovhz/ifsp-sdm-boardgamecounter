package br.edu.ifspsaocarlos.sdm.boardgamecounter;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.Timer;
import java.util.TimerTask;

public class CronometerActivity extends AppCompatActivity implements View.OnClickListener, CounterFragment.OnFragmentInteractionListener {

    private CounterFragment contadorFragment;
    private int configMinutes;
    private int configSeconds;
    private boolean counting;

    Button btnStart;
    Button btnRestart;

    private Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cronometer);

        this.configMinutes = 0;
        this.configSeconds = 0;
        this.counting = false;

        contadorFragment = CounterFragment.newInstance(configMinutes, configSeconds, false);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.contador_padrao, this.contadorFragment, "contador-padrao");

        transaction.commit();

        btnStart = findViewById(R.id.btn_cronometer_start);
        btnRestart = findViewById(R.id.btn_cronometer_restart);

        btnStart.setOnClickListener(this);
        btnRestart.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        if (isStartStopButtonClicked(v)) {

            if (isExecutingCronometer()) {
                timer.cancel();
                pararContagem();
            } else {
                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {
                        incrementTimer();
                    }
                };
                timer = new Timer();
                timer.schedule(task, 1000L, 1000L);
                iniciarContagem();

            }

        } else {
            if (R.id.btn_cronometer_restart == v.getId()) {
                if (isNotExecutinfCronometer()) {
                    resetCounters();
                }
            }
        }
    }

    private void resetCounters() {
        contadorFragment.reset(configMinutes, configSeconds);
    }

    private boolean isExecutingCronometer() {
        return counting;
    }

    private boolean isNotExecutinfCronometer() {
        return counting == false;
    }

    private boolean isStartStopButtonClicked(View v) {
        return R.id.btn_cronometer_start == v.getId();
    }

    private void incrementTimer() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                contadorFragment.incrementTimer();
            }
        });
    }

    private void iniciarContagem() {
        counting = true;
        btnStart.setText(R.string.pause);
        btnRestart.setEnabled(false);
    }

    private void pararContagem() {
        counting = false;
        btnStart.setText(R.string.iniciar);
        btnRestart.setEnabled(true);
    }

    @Override
    public void onTimerFinished() {

    }

    @Override
    public void onTimerClicked(CounterFragment fragment) {

    }
}
