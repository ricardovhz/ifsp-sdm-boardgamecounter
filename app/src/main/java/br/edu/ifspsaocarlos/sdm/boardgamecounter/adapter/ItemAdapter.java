package br.edu.ifspsaocarlos.sdm.boardgamecounter.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import br.edu.ifspsaocarlos.sdm.boardgamecounter.R;

/**
 * Created by ricardovhz on 02/12/17.
 */

public class ItemAdapter extends ArrayAdapter<String> {

    private LayoutInflater inflater;

    public ItemAdapter(@NonNull Context context, int resource, @NonNull List<String> objects) {
        super(context, resource, objects);
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.fragment_item, null);
            holder = new ViewHolder();
            holder.txtItemDescription = convertView.findViewById(R.id.txt_item_description);
            holder.btnRemover = convertView.findViewById(R.id.btn_remover);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final String itemText = getItem(position);

        holder.txtItemDescription.setText(itemText);
        holder.btnRemover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ItemAdapter.this.remove(itemText);
            }
        });
        return convertView;
    }

    static class ViewHolder {
        public TextView txtItemDescription;
        public Button btnRemover;
    }
}
